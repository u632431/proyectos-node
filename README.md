# Proyectos-node

Este repositorio cuenta con tres proyectos distintos, todos trabajados únicamente desde el backend:
1) Una aplicacion de consola que simula una "to-do list"
2) Una segunda aplicación de consola que nos permite checkear el clima en distintos lugares del mundo
3) Un proyecto que cuenta con servidor, almacenamiento en base de datos, manejo de APIrest y despliegue en mongoAtlas

## Getting started

Para los dos primeros proyectos, será necesario utilizar el comando "nodemon app" en nuestra terminal. Al ser aplicaciones de consola, las mismas aparecen dentro de la terminal.
El tercer proyecto cuenta con un README propio, donde se indica como hacer las distintas conexiones necesarias
