const { Schema, model } = require('mongoose');

//Con esto se maneja la coleccion de roles en el mongoCompas
const RoleSchema = Schema({
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio']
    }
});


module.exports = model( 'Role', RoleSchema );
