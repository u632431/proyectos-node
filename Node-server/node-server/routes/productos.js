const { Router } = require('express');
const { check } = require('express-validator');
const { validarJWT } = require('../middlewares/validar-jwt');
const { validarCampos } = require('../middlewares/validar-campos');
const { adminRole } = require('../middlewares/validar-roles');
const { crearProducto, obtenerProductos, obtenerProducto, actualizarProducto, borrarProducto } = require('../controllers/Productos');
const { existeProductoPorId } = require('../helpers/db-validators');

const router = Router();

//crear Producto - privado - cualquier persona con token valido
router.get('/', [ 
    validarJWT,
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('categoria', 'No es un id de Mongo').isMongoId(),
    check('id').custom( existeProductoPorId),
    validarCampos
 ],  crearProducto)

//obtener las Productos - publico
router.get('/', obtenerProductos)

//obtener una Producto por id - publico
router.get('/:id', [
    check('id', 'No es un id de Mongo válido').isMongoId(),
    check('id').custom( existeProductoPorId),
    validarCampos
] , obtenerProducto)

//actualizar- privado - cualquier persona con token valido
router.put('/:id', [
    validarJWT,
    check('id', 'No es un id de Mongo válido').isMongoId(),
    check('id').custom( existeProductoPorId),
    validarCampos
], actualizarProducto)

//Borrar una Producto - admin
router.put('/:id',[
    validarJWT,
    adminRole,
    check('id', 'No es un id de Mongo válido').isMongoId(),
    check('id').custom(existeProductoPorId),
    validarCampos
], borrarProducto)



module.exports= router