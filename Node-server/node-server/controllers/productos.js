const {response} = require('express');
const { Producto } = require('../models/producto');

//obtener Producto-populate
//actualizar Producto-populate
//borrar Producto-populate
const crearProducto = async(req, res) =>{

    const {estado, usuario, ...body} = req.body

    const ProductoDB = await Producto.findOne({ nombre: body.nombre });

    if (ProductoDB) {
        return res.status(400)-json({
            msg: `el Producto ${ ProductoDB.nombre}, ya existe`
        })
    }
    
    //Generar la data a guardar
    const data = {
        ...body,
        nombre: body.nombre.toUpperCase(),
        usuario: req.usuario._id
    }

    const Producto = new Producto( data );

    //Guardar DB
    await Producto.save();
    res.status(201).json(Producto)
}
//obtener Producto -apaginado-total-populate
//Population is the process of automatically replacing the specified paths in the document with document(s) from other collection(s).
const obtenerProductos = async(req, res) =>{
    const { limite = 5, desde = 0 } = req.query;
    const query = {estado: true};

    const [total, productos] = await Promise.all([
        Producto.countDocuments(query),
        Producto.find(query)
        .populate('usuario', 'nombre')
        .skip( Number( desde ) )
        .limit(Number( limite ))
    ])
    res.json({
        total,
        productos
    })
}

const obtenerProducto = async(req, res) =>{
    const {id} = req.params;
    const producto = await Producto.findById(id)
                                   .populate('usuario', 'nombre')
                                   .populate('categoria', 'nombre');

    res.json(producto)
}

const actualizarProducto = async(req, res) =>{
    const {id} =req.params;
    const {estado, usuario, ...data} = req.body;

    if( data.nombre){
      data.nombre = data.nombre.toUpperCase();  
    }
    
    data.usuario = req.usuario._id;

    const producto = await Producto.findByIdAndUpdate(id, data, {new: true});

    res.json( producto )
}

const borrarProducto = async(req, res) =>{
    const {id} = req.params;
    const productoBorrado = await Producto.findByIdAndUpdate(id, {estado: false}, {new: true});

    res.json( productoBorrado)
}
 
module.exports = {
    crearProducto,
    obtenerProductos,
    obtenerProducto,
    actualizarProducto,
    borrarProducto
}