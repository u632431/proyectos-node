const { validationResult } = require('express-validator');

const validarCampos = ( req, res, next ) => {

    const errors = validationResult(req);
    if( !errors.isEmpty() ){
        return res.status(400).json(errors);
    }
    //si lllega hasta este punto, que siga con el próximo middleware
    next();
}

module.exports = {
    validarCampos
}
