const jwt = require('jsonwebtoken')
const {response} = require('express')

const Usuario = require('../models/usuario')
//para leer los headers hay que leer la request. 
//El middleware se dispra con tres argumentos
const validarJWT = async(req, res = response, next) => {

    const token= req.header('x-token');

    if ( !token) {
        return response.status(401).json({
            msg: 'No hay token en la petición'
        })
    }

    try {
        const {uid}=  jwt.verify(token, process.env.SECRETORPRIVATEKEY);

        //leer el usuario que corresponde al uid
        const usuario= await Usuario.findById(uid)
       

        //Verificar que el uid no haya sido eliminado (que esté en false)
        if (!usuario.estado){
            return res.status(401).json({
                msg: 'Token no válido'
            })
        }
        req.usuario = usuario

        next()
    } catch {
        console.log(error)
        res.status(401).json({
            msg: 'Token no válido'
        })
    }

} 

module.exports = {
    validarJWT
}